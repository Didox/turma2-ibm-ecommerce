FROM maven
ARG JAR_FILE=target/*.jar
COPY . /tmp_build
WORKDIR /tmp_build
RUN mvn clean package -Dmaven.test.skip \
    && cp ${JAR_FILE} /app.jar \
    && rm -fr *
ENTRYPOINT ["java","-jar","/app.jar"]
